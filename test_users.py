import sqlite3

try:
    db = sqlite3.connect('winnie.db')
    
    cursor = db.cursor()

    cursor.execute('select * from users')
    for row in cursor:
        print('{}|{}|{}|{}|{}|{}|{}|{}|{}'.format(*row))
        
    db.commit()
    
    cursor.close()
    db.close()
except:
    print('Error')
    exit(1)